
let _documentId;
const name = "Wichteln";
const people = ["Mike", "Philipp", "Kalle", "Simon", "Jerermy", "Jan"];

// Initialize Firebase
var config = {
	apiKey: "AIzaSyDIu3KJM8DVCdHA8B7RaiFvfrHvRIicywA",
	authDomain: "pickbucket-1f4d3.firebaseapp.com",
	databaseURL: "https://pickbucket-1f4d3.firebaseio.com",
	projectId: "pickbucket-1f4d3",
	storageBucket: "pickbucket-1f4d3.appspot.com",
	messagingSenderId: "683695826431"
};
firebase.initializeApp(config);
const firestore = firebase.firestore();
const settings = {
	timestampsInSnapshots: true
};
firestore.settings(settings);
firestore.collection('buckets')
setTimeout(prepare, 100);

/**
 * 
 * @param {MouseEvent} e 
 */
async function pickAction(e) {
	if (!e.isTrusted) {
		e.preventDefault();
		return;
	}

	const stateJson = localStorage.getItem(_documentId);
	const state = stateJson ? JSON.parse(stateJson) : {
		partner: null
	}

	if (state.partner) {
		return;
	}

	const document = await getDocument(_documentId);
	const data = document.data();
	const number = await _.random(0, data.people.length - 1, false);

	const partner = data.people[number];
	data.people = data.people.filter(p => p != partner);

	await document.ref.set(data);

	state.partner = partner
	localStorage.setItem(_documentId, JSON.stringify(state));

	await updateState();
}

async function createDocument(value) {
	return await firestore.collection('buckets').add(value);
}

async function getDocument(id) {
	const snapshot = await firestore.collection('buckets').get();
	return snapshot.docs.find(doc => doc.id === id);
}

async function prepare() {

	let document;

	if (location.hash.length <= 1) {
		document = await createDocument({
			name,
			people
		});
		document = await getDocument(document.id);
		
		_documentId = document.id;
		window.history.replaceState({}, "", "#" + document.id);
		
	} else {

		_documentId = location.hash.substring(1);
		document = await getDocument(_documentId);
		
	}

	if (!document) {
		return alert("Eimer ist weg");
	}

	updateState();

}

async function updateState() {

	const stateJson = localStorage.getItem(_documentId);
	const state = stateJson ? JSON.parse(stateJson) : {
		partner: null
	}

	if (state.partner) {
		document.getElementById("pickAction").remove();
		document.getElementById("out_partner").textContent = `Du hast ${state.partner} gezogen.`;
	}

}


document.getElementById("pickAction").addEventListener("click", pickAction);